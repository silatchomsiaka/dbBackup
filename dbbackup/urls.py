from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import permission_required, login_required

from dbbackup.views import ChangeJobConfig, Home, JobConfigList, BackupList, test_db_connection,\
    test_destination_server_connection  # , JobConfigView
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns(
    '',
    # url(r'^$', Home.as_view(), name='home'),
    url(r'^jobConfigurationList$', permission_required("dbbackup.ik_setup_jobs")(JobConfigList.as_view()), name='jobconfig_list'),
    url(r'^jobConfig$', permission_required("dbbackup.ik_setup_jobs")(ChangeJobConfig.as_view()), name='change_jobconfig'),
    url(r'^jobConfig/(?P<object_id>[-\w]+)$', permission_required("dbbackup.ik_setup_jobs")(ChangeJobConfig.as_view()), name='change_jobconfig'),
    url(r'^backupList$', permission_required("dbbackup.ik_manage_backups")(BackupList.as_view()), name='backup_list'),
    url(r'^backupList/(?P<object_id>[-\w]+)$', permission_required("dbbackup.ik_manage_backups")(BackupList.as_view()), name='backup_list'),
    url(r'^dbServerValidation$', permission_required("dbbackup.ik_manage_backups")(test_db_connection), name='test_db_connection'),
    url(r'^destinationServerValidation$', permission_required("dbbackup.ik_manage_backups")(test_destination_server_connection), name='test_destination_server_connection'),

)


