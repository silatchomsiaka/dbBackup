from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext as _
from ikwen.core.utils import add_database, get_service_instance

from dbbackup.models import Backup, JobConfig


def create_permissions(db=None):
    service = get_service_instance()
    if not db:
        db = service.database
    add_database(db)
    backups_ct = ContentType.objects.get_for_model(Backup)
    Permission.objects.using(db).create(codename='ik_manage_backups', name=_('Manage backups'), content_type=backups_ct)
    jobs_ct = ContentType.objects.get_for_model(JobConfig)
    Permission.objects.using(db).create(codename='ik_setup_jobs', name=_('Setup Jobs'), content_type=jobs_ct)